package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class Data (

	@SerializedName("albums") val albums : List<Album>,
	@SerializedName("created_at") val created_at : String,
	@SerializedName("id") val id : Int,
	@SerializedName("title") val title : String
)