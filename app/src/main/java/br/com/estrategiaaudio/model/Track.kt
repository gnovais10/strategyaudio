package br.com.estrategiaaudio.model

data class Track(val id: Int, val title: String, val duration: Long, val track_url: String,
                 val created_at: String, val description : String, val teacher: String, val updated_at: String, var selected: Boolean = false)
