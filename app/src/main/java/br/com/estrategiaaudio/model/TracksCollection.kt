package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class TracksCollection (
    @SerializedName("data") val data : List<Track>,
    @SerializedName("error") val error : String,
    @SerializedName("meta") val meta : Meta
)