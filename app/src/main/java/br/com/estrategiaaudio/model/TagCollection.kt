package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class TagCollection (
	@SerializedName("data") val data : List<Data>,
	@SerializedName("error") val error : String,
	@SerializedName("meta") val meta : Meta
)