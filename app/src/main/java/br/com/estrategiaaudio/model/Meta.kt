package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class Meta (

	@SerializedName("page") val page : Int,
	@SerializedName("per_page") val per_page : Int,
	@SerializedName("total_items") val total_items : Int,
	@SerializedName("order") val order : String,
	@SerializedName("sort") val sort : String
)