package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class Tag(
    @SerializedName("created_at") val created_at : String,
    @SerializedName("id") val error : Int,
    @SerializedName("title") val meta : String)