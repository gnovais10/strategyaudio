package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class AlbumCollection (
	@SerializedName("data") val data : List<Album>,
	@SerializedName("error") val error : String,
	@SerializedName("meta") val meta : Meta
)