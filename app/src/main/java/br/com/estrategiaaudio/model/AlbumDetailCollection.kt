package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class AlbumDetailCollection (
	@SerializedName("data") val data : Album,
	@SerializedName("error") val error : String,
	@SerializedName("meta") val meta : Meta
)