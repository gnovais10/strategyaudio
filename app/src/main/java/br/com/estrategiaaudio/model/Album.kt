package br.com.estrategiaaudio.model

import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("created_at") val created_at : String,
    @SerializedName("description") val description : String,
    @SerializedName("id") val id : Int,
    @SerializedName("image_url") val image_url : String,
    @SerializedName("is_published") val is_published : Boolean,
    @SerializedName("published_at") val published_at : String,
    @SerializedName("tag_ids") val tag_ids : String,
    @SerializedName("tags") val tags : List<Tag>,
    @SerializedName("teachers") val teachers : List<String>,
    @SerializedName("title") val title : String,
    @SerializedName("track_ids") val track_ids : String,
    @SerializedName("tracks") val tracks : List<Track>,
    @SerializedName("updated_at") val updated_at : String,
    var liked: Boolean = false

)
