package br.com.estrategiaaudio.ui.main

import br.com.estrategiaaudio.model.Track
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.player.PlayerListener
import br.com.estrategiaaudio.player.PlayerManager
import br.com.estrategiaaudio.ui.base.BaseView

class MainActivityPresenter : MainActivityPresenterContract, PlayerListener {

    private lateinit var view: MainActivityView

    override fun attach(viewToAttach: BaseView, service: StrategyService) {
        view = viewToAttach as MainActivityView
        PlayerManager.playerListener = this
    }

    override fun playTrack(track: Track) {
        PlayerManager.playTrack(track)
    }

    override fun pauseOrResumeTrack() {
        PlayerManager.pauseOrResume()
    }

    override fun payerPrepared() {
        view.initializeSeekBar()
    }

    override fun playerPaused() {

    }
}