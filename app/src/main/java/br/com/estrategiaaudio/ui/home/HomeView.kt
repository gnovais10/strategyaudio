package br.com.estrategiaaudio.ui.home

import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.model.TagCollection
import br.com.estrategiaaudio.ui.base.BaseView

interface HomeView : BaseView {
    fun showAlbunsByTag(collections: TagCollection)
    fun favoriteAlbumSuccess(album: Album)
}