package br.com.estrategiaaudio.ui.base

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Toast
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.application.MainApplication
import br.com.estrategiaaudio.di.application.ApplicationComponent
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {
    @Inject
    lateinit var preferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val app = application as MainApplication
        app.getInjector().inject(this)
    }

    /**
     * Show snack message
     */
    fun showSnack(message: String) {
        val snack = Snackbar.make(findViewById(R.id.rootLayout), message, Snackbar.LENGTH_SHORT)
        snack.show()
    }

    /**
     * Show toast message
     */
    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * get the injector to inject dependencies
     */
    fun getInjector(): ApplicationComponent {
        val app = application as MainApplication
        return app.getInjector()
    }




}
