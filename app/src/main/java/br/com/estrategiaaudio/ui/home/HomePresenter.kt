package br.com.estrategiaaudio.ui.home

import android.util.Log
import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.model.AlbumDetailCollection
import br.com.estrategiaaudio.model.TagCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.base.BaseView
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class HomePresenter : HomePresenterContract {
    private lateinit var view: HomeView
    private lateinit var service: StrategyService

    /**
     * get the albuns of the home screen
     */
    override fun getAlbunsByTags() {
        //Here is a mock, instead of it, you will use retrofit to make the request to get the collections
        val albuns = service.getAlbums()
        albuns.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Response<TagCollection>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(listResponse: Response<TagCollection>) {
                    if (listResponse.isSuccessful) {
                        listResponse.body()?.apply {
                            view.showAlbunsByTag(this)
                        }
                    } else {
                        listResponse.raw().body()
                    }
                }

                override fun onError(e: Throwable) {
                    Log.e("getAlbmnsByTags", e.printStackTrace().toString())
                }
            })
    }

    override fun favoriteAlbum(album: Album) {

    }

    override fun attach(view: BaseView, service: StrategyService) {
        this.view = view as HomeView
        this.service = service
    }
}