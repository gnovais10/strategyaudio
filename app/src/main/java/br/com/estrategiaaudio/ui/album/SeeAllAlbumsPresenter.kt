package br.com.estrategiaaudio.ui.album

import br.com.estrategiaaudio.model.AlbumCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.base.BaseView
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class SeeAllAlbumsPresenter : SeeAlbumContract.Presenter {

    private lateinit var view: SeeAlbumContract.View
    private lateinit var service: StrategyService

    override fun getAlbums(tagId: Int) {
        val albums = service.getAllAlbumsByTag(tagId)
        albums.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Response<AlbumCollection>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(listResponse: Response<AlbumCollection>) {
                    if (listResponse.isSuccessful) {
                        listResponse.body()?.apply {
                            view.getAlbumsSuccess(this)
                        }
                    } else {
                        view.onError()
                    }
                }

                override fun onError(e: Throwable) {
                    view.onErrorConnection()
                }
            })
    }

    override fun attach(view: BaseView, service: StrategyService) {
        this.view = view as SeeAlbumContract.View
        this.service = service
    }
}