package br.com.estrategiaaudio.ui.base

import br.com.estrategiaaudio.network.StrategyService

interface BasePresenterContract {
    fun attach(view: BaseView, service: StrategyService)
}