package br.com.estrategiaaudio.ui.album

import br.com.estrategiaaudio.model.AlbumCollection
import br.com.estrategiaaudio.ui.base.BasePresenterContract
import br.com.estrategiaaudio.ui.base.BaseView

interface SeeAlbumContract {

    interface View : BaseView {
        fun getAlbumsSuccess(albumCollection: AlbumCollection)
    }

    interface Presenter : BasePresenterContract {
        fun getAlbums(tagId: Int)
    }
}