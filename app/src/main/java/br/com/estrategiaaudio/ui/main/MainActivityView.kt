package br.com.estrategiaaudio.ui.main

import android.support.v4.app.Fragment
import br.com.estrategiaaudio.ui.base.BaseView

interface MainActivityView : BaseView {
    fun openFragment(fragment: Fragment)
    fun initializeSeekBar()
    fun playerPaused()
}