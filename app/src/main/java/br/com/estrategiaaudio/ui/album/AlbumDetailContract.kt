package br.com.estrategiaaudio.ui.album

import br.com.estrategiaaudio.model.AlbumDetailCollection
import br.com.estrategiaaudio.model.TracksCollection
import br.com.estrategiaaudio.ui.base.BasePresenterContract
import br.com.estrategiaaudio.ui.base.BaseView

interface AlbumDetailContract {

    interface View : BaseView {
        fun albumsDetail(albumDetailCollection: AlbumDetailCollection)
        fun tracks(tracksCollection: TracksCollection)
    }

    interface Presenter : BasePresenterContract {
        fun getAlbumsDetail(albumId: Int)
        fun getTracks(albumId: Int)
    }
}