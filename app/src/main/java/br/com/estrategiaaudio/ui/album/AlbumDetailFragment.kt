package br.com.estrategiaaudio.ui.album

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.adapters.TrackAdapter
import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.model.AlbumDetailCollection
import br.com.estrategiaaudio.model.Track
import br.com.estrategiaaudio.model.TracksCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.main.MainActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_album_detail.*
import retrofit2.Retrofit
import javax.inject.Inject

private const val ALBUM_ID = "ALBUM_ID"


class AlbumDetailFragment : Fragment(), AlbumDetailContract.View, TrackAdapter.OnItemClickListener {

    @Inject
    lateinit var presenter: AlbumDetailPresenter

    @Inject
    lateinit var retrofit: Retrofit

    private lateinit var act: MainActivity
    private var albumId: Int = 0

    private lateinit var album: Album

    companion object {
        @JvmStatic
        fun newInstance(tagId: Int) =
            AlbumDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ALBUM_ID, tagId)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            albumId = it.getInt(ALBUM_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_album_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initControls()
    }

    override fun initControls() {
        act = activity as MainActivity
        act.getInjector().inject(this)

        presenter.attach(this, retrofit.create(StrategyService::class.java))
        presenter.getAlbumsDetail(albumId)
        presenter.getTracks(albumId)
        configureToolbar()
    }

    private fun configureToolbar(){
        album_toolbar.setOnClickListener {
            act.onBackPressed()
        }
    }

    override fun onClickItem(track: Track) {
        act.presenter.playTrack(track)
        act.refreshPlayer(track, album.image_url)
    }

    override fun albumsDetail(albumDetailCollection: AlbumDetailCollection){
        album = albumDetailCollection.data
        album_name.text = albumDetailCollection.data.title
        teacher_name.text = albumDetailCollection.data.teachers.toString()
            .replace("[", "").replace("]", "")
        description.text = albumDetailCollection.data.description

        Glide.with(context).load(albumDetailCollection.data.image_url).into(album_image)
    }

    override fun tracks(tracksCollection: TracksCollection) {
        track_recycler.adapter = TrackAdapter(act, tracksCollection.data, this)
        track_recycler.layoutManager = LinearLayoutManager(act)

        playAudioClickListener(tracksCollection)
    }

    private fun playAudioClickListener(tracksCollection: TracksCollection) {
        buttonPlayAudio.setOnClickListener {
            act.presenter.playTrack(tracksCollection.data.first())
            (track_recycler.adapter as TrackAdapter).selectSingle(tracksCollection.data.first())
        }
    }

    override fun onError() {
       act.showSnack(getString(R.string.error_message))
    }

    override fun onErrorConnection() {
        act.showSnack(getString(R.string.error_message))
    }
}
