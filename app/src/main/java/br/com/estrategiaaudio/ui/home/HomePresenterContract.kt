package br.com.estrategiaaudio.ui.home

import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.ui.base.BasePresenterContract

interface HomePresenterContract : BasePresenterContract {

    fun getAlbunsByTags()
    fun favoriteAlbum(album: Album)
}