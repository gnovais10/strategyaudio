package br.com.estrategiaaudio.ui.album

import android.util.Log
import br.com.estrategiaaudio.model.AlbumDetailCollection
import br.com.estrategiaaudio.model.TracksCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.base.BaseView
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class AlbumDetailPresenter : AlbumDetailContract.Presenter {

    private lateinit var view: AlbumDetailContract.View
    private lateinit var service: StrategyService

    override fun getAlbumsDetail(albumId: Int) {
        val albums = service.getAlbumsDetail(albumId)
        albums.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Response<AlbumDetailCollection>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(listResponse: Response<AlbumDetailCollection>) {
                    if (listResponse.isSuccessful) {
                        listResponse.body()?.apply {
                            view.albumsDetail(this)
                        }
                    } else {
                        listResponse.raw().body()
                    }
                }

                override fun onError(e: Throwable) {
                    Log.e("getAlbumsDetail", e.printStackTrace().toString())
                }
            })
    }

    override fun getTracks(albumId: Int) {
        val tracks  = service.getTracks(albumId)
            tracks.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Response<TracksCollection>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(listResponse: Response<TracksCollection>) {
                    if (listResponse.isSuccessful) {
                        listResponse.body()?.apply {
                            view.tracks(this)
                        }
                    } else {
                        view.onError()
                    }
                }

                override fun onError(e: Throwable) {
                    view.onErrorConnection()
                }
            })

    }

    override fun attach(view: BaseView, service: StrategyService) {
        this.view = view as AlbumDetailContract.View
        this.service = service
    }

}