package br.com.estrategiaaudio.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.adapters.AlbunsByTagAdapter
import br.com.estrategiaaudio.adapters.HomeTagsAdapters
import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.model.Data
import br.com.estrategiaaudio.model.TagCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.album.AlbumDetailFragment
import br.com.estrategiaaudio.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_home.view.*
import retrofit2.Retrofit
import javax.inject.Inject


class HomeFragment : Fragment(), HomeView, AlbunsByTagAdapter.OnItemClickListener {

    @Inject
    lateinit var presenter: HomePresenter

    @Inject
    lateinit var retrofit: Retrofit

    private lateinit var rootView: View

    private lateinit var act: MainActivity


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_home, container, false)
        initControls()
        return rootView
    }

    /**
     * Init the controls of the fragment
     */
    override fun initControls() {
        act = activity as MainActivity
        act.getInjector().inject(this)

        presenter.attach(this, retrofit.create(StrategyService::class.java))
        presenter.getAlbunsByTags()
    }

    /**
     * show the albuns by tag on home when it is loaded
     */
    override fun showAlbunsByTag(collection: TagCollection) {
        val dataNotAlbumEmpty = mutableListOf<Data>()

        collection.data.forEach {
            if (!it.albums.isNullOrEmpty()){
                dataNotAlbumEmpty.add(it)
            }
        }

        val tagsAdapter = HomeTagsAdapters(context!!, dataNotAlbumEmpty, this)

        with(rootView.tags_agroupment_recycler) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
            adapter = tagsAdapter
        }

        tagsAdapter.observable.subscribe {
            presenter.favoriteAlbum(it)
        }
    }

    override fun onClickItem(tagId: Int) {
        act.openFragment(AlbumDetailFragment.newInstance(tagId))
    }

    /**
     * show the message when the album is successfully favorited
     */
    override fun favoriteAlbumSuccess(album: Album) {
        act.showToast("O curso ${album.description} foi adicionado à sua biblioteca")
    }

    /**
     * shows a error message
     */
    override fun onError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * shows a error connection message
     */
    override fun onErrorConnection() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
