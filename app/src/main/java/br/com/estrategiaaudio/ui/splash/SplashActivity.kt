package br.com.estrategiaaudio.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.ui.base.BaseActivity
import br.com.estrategiaaudio.ui.main.MainActivity
import br.com.estrategiaaudio.ui.onBoarding.OnBoardingActivity
import br.com.estrategiaaudio.util.Constants

class SplashActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        countTimeSplash()
    }

    /**
     * counting the time showing the logo before start the app
     */
    fun countTimeSplash() {
        val handler = Handler()

        handler.postDelayed(Runnable {
            if (preferences.getBoolean(Constants.FIRST_TIME, true)) {
                goToOnBoardingScreen()
            } else {
                goToAppScreen()
            }

        }, 3000)
    }

    /**
     * go to onboarding screen
     */
    fun goToOnBoardingScreen() {
        val intent = Intent(this, OnBoardingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        val shared = preferences.edit().putBoolean(Constants.FIRST_TIME, false)
        shared.apply()
        startActivity(intent)
    }

    /**
     * go to the app screen
     */
    fun goToAppScreen() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
