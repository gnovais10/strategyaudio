package br.com.estrategiaaudio.ui.main

import br.com.estrategiaaudio.model.Track
import br.com.estrategiaaudio.ui.base.BasePresenterContract

interface MainActivityPresenterContract : BasePresenterContract {
    fun playTrack(track: Track)
    fun pauseOrResumeTrack()
    fun playerPaused()
}