package br.com.estrategiaaudio.ui.main

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.model.Track
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.player.PlayerManager
import br.com.estrategiaaudio.ui.base.BaseActivity
import br.com.estrategiaaudio.ui.home.HomeFragment
import br.com.estrategiaaudio.ui.library.LibraryFragment
import br.com.estrategiaaudio.ui.search.SearchFragment
import br.com.estrategiaaudio.ui.settings.SettingsFragment
import br.com.estrategiaaudio.util.ChangeMiliToHour
import br.com.estrategiaaudio.util.Mocks
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.track_layout.*
import retrofit2.Retrofit
import javax.inject.Inject
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


class MainActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener, MainActivityView {

    @Inject
    lateinit var presenter: MainActivityPresenter

    lateinit var runnable: Runnable
    var handler = Handler()

    var chosenTrack = Mocks.track1 //This is only for mock

    @Inject
    lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)
        getInjector().inject(this)
        initControls()
    }

    /**
     * init controls and the widgets
     */
    override fun initControls() {
        bottom_navigation.setOnNavigationItemSelectedListener(this)
        openFragment(HomeFragment())
        bottomSheetCallBack()
        changeStatusBarColor()
        organizeMediaPlayerEvents()
        presenter.attach(this, retrofit.create(StrategyService::class.java))
    }

    /**
     * change the status bar color
     */
    fun changeStatusBarColor() {
        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.setStatusBarColor(Color.WHITE)
    }

    /**
     * prepare the callback for bottomSheet
     */
    fun bottomSheetCallBack() {
        val bottomSheetBehavior = BottomSheetBehavior.from(track_layout)
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                bottom_navigation.animate().translationY(slideOffset * bottom_navigation.height).setDuration(0)
                        .start()
                trackbar.alpha = 1f - slideOffset
                track_toolbar.alpha = slideOffset
            }
        })

        close.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    /**
     * bottom navigation listener
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home_bottom_button -> {
                openFragment(HomeFragment())
            }
            R.id.search_bottom_button -> {
                openFragment(SearchFragment())
            }
            R.id.library_bottom_button -> {
                openFragment(LibraryFragment())
            }
            R.id.settings_bottom_button -> {
                openFragment(SettingsFragment())
            }
        }
        return true
    }

    fun organizeMediaPlayerEvents() {
        play_pause.setOnClickListener {
            if (PlayerManager.playerState == PlayerManager.PlayerState.STOPPED) {
                presenter.playTrack(chosenTrack)
            } else {
                presenter.pauseOrResumeTrack()
            }

        }

        mini_play_pause.setOnClickListener {
            if (PlayerManager.playerState == PlayerManager.PlayerState.STOPPED) {
                presenter.playTrack(chosenTrack)

            } else {
                presenter.pauseOrResumeTrack()
            }
        }
    }

    /**
     * initialize seekbar with the music player
     */
    override fun initializeSeekBar() {
        musicSeekBar.max = PlayerManager.mediaPlayer.duration
        musicSeekBarMini.max = PlayerManager.mediaPlayer.duration
        remaining_time.text = ChangeMiliToHour.transform(PlayerManager.mediaPlayer.duration.toLong())

        runnable = Runnable {
            musicSeekBar.progress = PlayerManager.mediaPlayer.currentPosition
            musicSeekBarMini.progress = PlayerManager.mediaPlayer.currentPosition
            current_time.text = ChangeMiliToHour.transform(PlayerManager.mediaPlayer.currentPosition.toLong())
            handler.postDelayed(runnable, 1000)
        }

        handler.postDelayed(runnable, 1000)

        musicSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                PlayerManager.mediaPlayer.seekTo(seekBar.progress)
                current_time.text = ChangeMiliToHour.transform(PlayerManager.mediaPlayer.currentPosition.toLong())
            }
        })

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0 ){
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * open the fragment from bottom navigation
     */
    override fun openFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.addToBackStack(null)
        ft.replace(R.id.container_main, fragment)
        ft.commit()
    }

    /**
     * Some request error
     */
    override fun onError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Action when some connection error happens
     */
    override fun onErrorConnection() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun playerPaused() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun refreshPlayer(track: Track, albumUrl: String){
         track_bottom_title.text = track.title
        track_bottom_course.text = track.teacher
        Glide.with(this).load(albumUrl).into(album_image)
    }
}
