package br.com.estrategiaaudio.ui.album

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.adapters.SeeAllAlbumsAdapter
import br.com.estrategiaaudio.model.AlbumCollection
import br.com.estrategiaaudio.network.StrategyService
import br.com.estrategiaaudio.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_see_all_albums.*
import retrofit2.Retrofit
import javax.inject.Inject

private const val TAG_ID = "TAG_ID"


class SeeAllAlbumsFragment : Fragment(), SeeAlbumContract.View, SeeAllAlbumsAdapter.OnItemClickListener {

    private var tagId: Int = 0

    @Inject
    lateinit var presenter: SeeAllAlbumsPresenter

    @Inject
    lateinit var retrofit: Retrofit

    private lateinit var act: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tagId = it.getInt(TAG_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_see_all_albums, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initControls()
    }

    private fun configureToolbar() {
        album_toolbar.setOnClickListener {
            act.onBackPressed()
        }
    }

    override fun initControls() {
        act = activity as MainActivity
        act.getInjector().inject(this)

        presenter.attach(this, retrofit.create(StrategyService::class.java))
        presenter.getAlbums(tagId)
        configureToolbar()
    }

    override fun onErrorConnection() {
        act.showSnack(getString(R.string.error_message))
    }

    override fun onError() {
        act.showSnack(getString(R.string.error_message))
    }

    override fun onClickItem(albumId: Int) {
        act.openFragment(AlbumDetailFragment.newInstance(albumId))
    }

    override fun getAlbumsSuccess(albumCollection: AlbumCollection) {
        see_all_albums_recycler.adapter = SeeAllAlbumsAdapter(act, albumCollection.data, this)
        see_all_albums_recycler.layoutManager = LinearLayoutManager(act)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
            SeeAllAlbumsFragment().apply {
                arguments = Bundle().apply {
                    putInt(TAG_ID, param1)
                }
            }
    }
}
