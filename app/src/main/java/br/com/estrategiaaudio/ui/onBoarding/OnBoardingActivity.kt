package br.com.estrategiaaudio.ui.onBoarding

import android.content.Intent
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_on_boarding.*
import kotlinx.android.synthetic.main.fragment_on_boarding.view.*

class OnBoardingActivity : AppCompatActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    val LAST_POSITION = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        startPageAdapter()
    }

    /**
     * start the adapter listener function
     */
    fun startPageAdapter() {
        val dots = listOf<ImageView>(dot_1, dot_2, dot_3, dot_4)
        dot_1.setImageDrawable(getDrawable(R.drawable.selected_circle_onboarding))

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        container.adapter = mSectionsPagerAdapter
        container.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                for (dot in dots) {
                    if (dot == dots[position]) {
                        dot.setImageDrawable(getDrawable(R.drawable.selected_circle_onboarding))

                    } else {
                        dot.setImageDrawable(getDrawable(R.drawable.unselected_circle_onboarding))

                    }
                }
            }

        })

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_on_boarding, menu)
        return true
    }


    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return PlaceholderFragment.newInstance(position + 1)
        }

        override fun getCount(): Int {
            return 4
        }
    }

    class PlaceholderFragment : Fragment() {

        private var sectionNumberChosen = 0
        private lateinit var rootView: View

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            rootView = inflater.inflate(R.layout.fragment_on_boarding, container, false)
            sectionNumberChosen = this.arguments!!.getInt(ARG_SECTION_NUMBER)
            organizeWidgets()

            return rootView
        }

        fun organizeWidgets() {
            when (sectionNumberChosen) {
                1 -> {
                    rootView.title_onBoarding.text = getString(R.string.onBoarding_title_1)
                    rootView.text_onboarding.text = getString(R.string.onBoarding_text_1)
                    rootView.start_app_layout.visibility = View.GONE
                    rootView.icon_onboarding.setImageDrawable(context!!.getDrawable(R.drawable.onboarding_icon_one))
                }
                2 -> {
                    rootView.title_onBoarding.text = getString(R.string.onBoarding_title_2)
                    rootView.text_onboarding.text = getString(R.string.onBoarding_text_2)
                    rootView.start_app_layout.visibility = View.GONE
                    rootView.icon_onboarding.setImageDrawable(context!!.getDrawable(R.drawable.onboarding_icon_two))
                }
                3 -> {
                    rootView.title_onBoarding.text = getString(R.string.onBoarding_title_3)
                    rootView.text_onboarding.text = getString(R.string.onBoarding_text_3)
                    rootView.start_app_layout.visibility = View.GONE
                    rootView.icon_onboarding.setImageDrawable(context!!.getDrawable(R.drawable.onboarding_icon_three))
                }
                4 -> {
                    rootView.title_onBoarding.text = getString(R.string.onBoarding_title_4)
                    rootView.text_onboarding.text = getString(R.string.onBoarding_text_4)
                    rootView.start_app_layout.visibility = View.VISIBLE
                    rootView.icon_onboarding.setImageDrawable(context!!.getDrawable(R.drawable.onboarding_icon_four))

                    rootView.start_app_layout.setOnClickListener {
                        val intent = Intent(context, LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

                        startActivity(intent)
                    }
                }
            }
        }

        companion object {
            private val ARG_SECTION_NUMBER = "section_number"
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }


}
