package br.com.estrategiaaudio.ui.login

import android.os.Bundle
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.ui.base.BaseActivity

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}
