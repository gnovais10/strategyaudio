package br.com.estrategiaaudio.ui.base

interface BaseView {
    fun initControls()
    fun onError()
    fun onErrorConnection()
}