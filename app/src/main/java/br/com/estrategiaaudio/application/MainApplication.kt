package br.com.estrategiaaudio.application

import android.app.Application
import br.com.estrategiaaudio.di.application.ApplicationComponent
import br.com.estrategiaaudio.di.application.ApplicationModule
import br.com.estrategiaaudio.di.application.DaggerApplicationComponent


class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    fun getInjector(): ApplicationComponent {
        val injector = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        return injector
    }


}