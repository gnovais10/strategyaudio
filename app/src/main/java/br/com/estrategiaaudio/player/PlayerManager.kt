package br.com.estrategiaaudio.player

import android.media.MediaPlayer
import br.com.estrategiaaudio.model.Track
import java.lang.Exception
import java.lang.RuntimeException
import java.lang.reflect.InvocationTargetException

object PlayerManager {

    lateinit var mediaPlayer: MediaPlayer
    lateinit var playerListener: PlayerListener
    private var length = 0
    var playerState = PlayerState.STOPPED

    /**
     * prepare and instantiate the media player
     */
    init {
        mediaPlayer = MediaPlayer().apply {
            setOnPreparedListener {
                start()
                playerState = PlayerState.PLAYING
                playerListener.payerPrepared()
            }
            setOnCompletionListener {
                reset()
                PlayerState.STOPPED
            }
        }
    }

    /**
     * play the trackg
     */
    fun playTrack(track: Track) {
        mediaPlayer.run {
            reset()

            try {
                setDataSource(track.track_url)
                prepareAsync()
            } catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    /**
     * pause or resume the track
     */
    fun pauseOrResume() {
        if (mediaPlayer.isPlaying) {
            playerState = PlayerState.PAUSED
            mediaPlayer.pause()
            length = mediaPlayer.currentPosition
            playerListener.playerPaused()
        } else {
            playerState = PlayerState.PLAYING
            mediaPlayer.seekTo(length)
            mediaPlayer.start()
        }
    }

    enum class PlayerState {
        STOPPED, PAUSED, PLAYING
    }
}