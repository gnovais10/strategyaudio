package br.com.estrategiaaudio.player

interface PlayerListener {

    fun payerPrepared()
    fun playerPaused()
}