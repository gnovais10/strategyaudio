package br.com.estrategiaaudio.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.model.Track
import kotlinx.android.synthetic.main.track_item.view.*


class TrackAdapter(val context: Context, private val tracks: List<Track>, private val listenerItemClick : TrackAdapter.OnItemClickListener) :
        RecyclerView.Adapter<TrackAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onClickItem(track : Track)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.track_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val track = tracks.get(position)
        holder.title.text = track.title
        holder.duration.text = track.duration.toString()

        changeLayoutSelectedSingle(track, holder)

        holder.track_content.setOnClickListener {
            listenerItemClick.onClickItem(track)
            holder.title.setTextColor(context.getColor(R.color.blue_theme))
            selectSingle(track)
        }
    }

    private fun changeLayoutSelectedSingle(track: Track, holder: ViewHolder) {
        tracks.forEach { t ->
            if (t == track && t.selected) {
                holder.title.setTextColor(context.getColor(R.color.blue_theme))
            }
        }
    }

    fun selectSingle(track: Track) {
        track.selected = true

        tracks.forEach { t ->
            if (t != track) {
                t.selected = false
            } else {
                track.selected = true
            }
        }

        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val title = view.title
        val duration = view.duration
        val track_content = view.track_content

    }
}