package br.com.estrategiaaudio.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.model.Album
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.see_all_albums_item.view.*


class SeeAllAlbumsAdapter(
    val context: Context,
    val albuns: List<Album>,
    private val listenerItemClick : OnItemClickListener
) :
    RecyclerView.Adapter<SeeAllAlbumsAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onClickItem(albumId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.see_all_albums_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return albuns.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = albuns.get(position)
        holder.albumName.text = album.title
        holder.teacher_name.text = album.teachers.toString()

        Glide.with(context).load(album.image_url).into(holder.image)

        holder.albumItemContent.setOnClickListener {
            listenerItemClick.onClickItem(album.id)
        }

    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val albumItemContent = view.albumItemContent
        val albumName = view.album_name
        val teacher_name = view.teacher_name
        var image = view.album_image

    }

}