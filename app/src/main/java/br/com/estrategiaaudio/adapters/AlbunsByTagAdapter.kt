package br.com.estrategiaaudio.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.model.Album
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.albuns_collections_item.view.*



class AlbunsByTagAdapter(val context: Context, val albuns: List<Album>, val parentHome: HomeTagsAdapters, private val listenerItemClick : OnItemClickListener) :
        RecyclerView.Adapter<AlbunsByTagAdapter.ViewHolder>() {

    val publisher = PublishSubject.create<Album>()
    val clickEvent: Observable<Album> = publisher

    interface OnItemClickListener {
        fun onClickItem(tagId : Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        listenToParent(parentHome)
        val view = LayoutInflater.from(context).inflate(R.layout.albuns_collections_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return albuns.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = albuns.get(position)
        holder.albumName.text = album.description
        holder.teacherName.text = album.teachers.requireNoNulls().first()
        Glide.with(context).load(R.drawable.default_image).into(holder.image)

        Glide.with(context).load(album.image_url).into(holder.image)

        holder.albumItemContent.setOnClickListener {
            listenerItemClick.onClickItem(album.id)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val albumName = view.album_name
        val image = view.album_image
        val teacherName = view.teacher_name
        val albumItemContent = view.albumItemContent

        var likeLayout = view.like_card_view

    }

    /**
     * draw or erase the heart icon to indicate if the album was liked or not
     */
    fun printLiked(liked: Boolean, holder: ViewHolder) {
        if (liked) {
            holder.likeLayout.setCardBackgroundColor(context.getColor(R.color.alpha_red))
        } else {
            holder.likeLayout.setCardBackgroundColor(context.getColor(R.color.alpha_dark))
        }
    }


    /**
     * listen the main adapter to send information if some album was liked
     */
    fun listenToParent(parent: HomeTagsAdapters) {
        parent.observable.subscribe {
            changeLikesFromOtherTag(it)
        }
    }

    /**
     * change the likes from another tag and check if there is the same album to update the like
     */
    fun changeLikesFromOtherTag(albumLiked: Album) {
        for (album in albuns) {
            if (albumLiked.id == album.id) {
                album.liked = albumLiked.liked
            }
        }

        notifyDataSetChanged()
    }


}