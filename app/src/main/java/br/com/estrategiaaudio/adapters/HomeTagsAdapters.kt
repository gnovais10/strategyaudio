package br.com.estrategiaaudio.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.estrategiaaudio.R
import br.com.estrategiaaudio.model.Album
import br.com.estrategiaaudio.model.Data
import br.com.estrategiaaudio.ui.album.SeeAllAlbumsFragment
import br.com.estrategiaaudio.ui.main.MainActivity
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.tags_informations_item.view.*


class HomeTagsAdapters(val context: Context, val tagCollections: List<Data>, private val listenerItemClick : AlbunsByTagAdapter.OnItemClickListener) :
        RecyclerView.Adapter<HomeTagsAdapters.ViewHolder>() {

    val publishSubject = PublishSubject.create<Album>()
    val observable: Observable<Album> = publishSubject

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.tags_informations_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tagCollections.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val collection = tagCollections.get(position)
        holder.tagname.text = collection.title

        val albunsAdapter = AlbunsByTagAdapter(context, collection.albums, this, listenerItemClick)

        with(holder.albuns) {
            setHasFixedSize(true)
            adapter = albunsAdapter
            layoutManager = android.support.v7.widget.LinearLayoutManager(
                    context,
                    android.support.v7.widget.LinearLayoutManager.HORIZONTAL,
                    false
            )
            addItemDecoration(br.com.estrategiaaudio.util.EqualSpacingItemDecoration(16, 0))
        }

        albunsAdapter.clickEvent.subscribe {
            checkIfSomeAlbumWasChecked(it) //when some event from favorite albuns is changed but the album data is not appeared yet
            publishSubject.onNext(it) //when some album collection was already shown on this adapter and must updte if there is some album with the same id
        }

        holder.see_all.setOnClickListener {
            (context as MainActivity).openFragment(SeeAllAlbumsFragment.newInstance(collection.id))
        }

        holder.tag_view.setOnClickListener {
            (context as MainActivity).openFragment(SeeAllAlbumsFragment.newInstance(collection.id))
        }

    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tagname = view.tagName
        val albuns = view.albuns_collections
        val see_all = view.see_all
        val tag_view = view.tag_view

    }

    /**
     * this function checks if the same album by another tag album list was checked to crete coerence between same albuns checked by different tags
     */
    fun checkIfSomeAlbumWasChecked(album: Album) {
        for (tag in tagCollections) {
            for (albumFromTags in tag.albums) {
                if (album.id == albumFromTags.id) {
                    albumFromTags.liked = album.liked
                }
            }
        }


        // notifyDataSetChanged()

    }
}