package br.com.estrategiaaudio.network

import br.com.estrategiaaudio.model.AlbumCollection
import br.com.estrategiaaudio.model.AlbumDetailCollection
import br.com.estrategiaaudio.model.TagCollection
import br.com.estrategiaaudio.model.TracksCollection
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StrategyService {

    @GET("tags/latest?filters={%is_published:true}")
    fun getAlbums() : Single<Response<TagCollection>>

    @GET("albums?")
    fun getAllAlbumsByTag(@Query("tag_id") tagId : Int, @Query("is_published") isPublished : Boolean = true) : Single<Response<AlbumCollection>>

    @GET("albums/{id}")
    fun getAlbumsDetail(@Path("id") albumId : Int) : Single<Response<AlbumDetailCollection>>

    @GET("tracks?")
    fun getTracks(@Query("album_id") albumId : Int) : Single<Response<TracksCollection>>

}