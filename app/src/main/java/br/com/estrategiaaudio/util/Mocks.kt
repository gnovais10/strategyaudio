package br.com.estrategiaaudio.util

import br.com.estrategiaaudio.model.Teacher
import br.com.estrategiaaudio.model.Track

class Mocks {


    companion object {

        //mocks of tracks url
        val urlTrack1 = "http://urban180.com/wp-content/uploads/2017/09/olaide-Bambi_urban180.com_-1.mp3"
        val urlTrack2 = "http://urban180.com/wp-content/uploads/2018/01/Evidence-.mp3"
        val urlTrack3 = "http://urban180.com/wp-content/uploads/2017/08/01-Na-So.mp3"
        val urlTrack4 =
            "http://urban180.com/wp-content/uploads/2017/09/05.-Lamboginny-_-Read-my-lips-ft-Olamide-Prod.-by-STO_urban180.com_.mp3"
        val urlTrack5 = "http://urban180.com/wp-content/uploads/2018/11/Wizkid_-_Sade-.mp3"
        val urlTrack6 = "http://urban180.com/wp-content/uploads/2018/09/Lil_Uzi_Vert_-_1600.mp3"
        val urlTrack7 = "http://urban180.com/wp-content/uploads/2017/07/Orezi-Cooking-Pot.mp3"

        // mocks of track names
        val trackName1 = "Acentuação"
        val trackName2 = "Pronomes"
        val trackName3 = "Análise sintática"
        val trackName4 = "Conjunção"
        val trackName5 = "Preposição"
        val trackName6 = "Substantivos compostos"
        val trackName7 = "Interjeição"

        //mocks of teachers
        val teacher = Teacher(1, "Roberval Júnior")

        //mock logo estrategia
        val imageUrl = "https://i.ibb.co/JQzDTGk/andre.jpg"

        //tracks mocks
        val track1 = Track(1, trackName1, 30000, urlTrack1, "", "", "","")
        val track2 = Track(2, trackName2, 30000, urlTrack2, "", "", "","")
        val track3 = Track(3, trackName3, 30000, urlTrack3, "", "", "","")
        val track4 = Track(4, trackName4, 30000, urlTrack4, "", "", "","")
        val track5 = Track(5, trackName5, 30000, urlTrack5, "", "", "","")
        val track6 = Track(6, trackName6, 30000, urlTrack6, "", "", "","")
        val track7 = Track(7, trackName7, 30000, urlTrack7, "", "", "","")

        val trackList = listOf(track1, track2, track3, track4, track5, track6, track7)

       /* //albuns
        val album1 = Album(1, "Português para TRE", trackList, teacher, imageUrl, true)
        val album2 = Album(2, "Português para TRE", trackList, teacher, imageUrl, true)
        val album3 = Album(3, "Português para TRE", trackList, teacher, imageUrl, false)
        val album4 = Album(4, "Português para TRE", trackList, teacher, imageUrl, false)
        val album5 = Album(5, "Português para TRE", trackList, teacher, imageUrl, false)

        val albunsTRE = listOf(album1, album2, album3, album4, album5)

        val album6 = Album(6, "Português para TRT", trackList, teacher, imageUrl, true)
        val album7 = Album(7, "Português para TRT", trackList, teacher, imageUrl, true)
        val album8 = Album(8, "Português para TRT", trackList, teacher, imageUrl, false)
        val album9 = Album(9, "Português para TRT", trackList, teacher, imageUrl, false)
        val album10 = Album(10, "Português para TRT", trackList, teacher, imageUrl, false)
        val album11 = Album(11, "Português para TRT", trackList, teacher, imageUrl, false)

        val albunsTRT = listOf(album6, album7, album8, album9, album10, album11)

        val album12 = Album(12, "Português para ABIN", trackList, teacher, imageUrl, true)
        val album13 = Album(13, "Português para ABIN", trackList, teacher, imageUrl, false)
        val album14 = Album(14, "Português para ABIN", trackList, teacher, imageUrl, false)
        val album15 = Album(15, "Português para ABIN", trackList, teacher, imageUrl, false)
        val album16 = Album(16, "Português para ABIN", trackList, teacher, imageUrl, false)
        val album17 = Album(17, "Português para ABIN", trackList, teacher, imageUrl, false)

        val albunsABIN = listOf(album12, album13, album14, album15, album16, album17)

        val album18 = Album(1, "Português para TRE", trackList, teacher, imageUrl, true)
        val album19 = Album(19, "Português para Receita", trackList, teacher, imageUrl, false)
        val album20 = Album(20, "Português para Receita", trackList, teacher, imageUrl, false)
        val album21 = Album(21, "Português para Receita", trackList, teacher, imageUrl, false)
        val album22 = Album(22, "Português para Receita", trackList, teacher, imageUrl, false)
        val album23 = Album(23, "Português para Receita", trackList, teacher, imageUrl, false)

        val albunsReceita = listOf(album18, album19, album20, album21, album22, album23)

        //mocks for group by tags
        val tagCollectionDestaques = TagCollection("Destaques", albunsTRE)
        val tagCollectionTRE = TagCollection("TRE", albunsTRE)
        val tagCollectionTRT = TagCollection("TRT", albunsTRT)
        val tagCollectionABIN = TagCollection("ABIN", albunsABIN)
        val tagCollectionReceita = TagCollection("Receita Federal", albunsReceita)

        val listOfTagCollection =
            listOf(tagCollectionDestaques, tagCollectionTRE, tagCollectionTRT, tagCollectionABIN, tagCollectionReceita)*/

    }
}