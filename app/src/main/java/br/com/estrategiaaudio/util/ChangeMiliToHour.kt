package br.com.estrategiaaudio.util

import java.util.concurrent.TimeUnit


class ChangeMiliToHour {

    companion object {
        fun transform(time: Long): String {
            val time = String.format("%02d : %02d", TimeUnit.MILLISECONDS.toMinutes(time),
                    TimeUnit.MILLISECONDS.toSeconds(time) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)))

            return time
        }

    }
}