package br.com.estrategiaaudio.di.application

import dagger.Component
import br.com.estrategiaaudio.di.presenters.PresentersModule
import br.com.estrategiaaudio.ui.album.AlbumDetailFragment
import br.com.estrategiaaudio.ui.album.SeeAllAlbumsFragment
import br.com.estrategiaaudio.ui.base.BaseActivity
import br.com.estrategiaaudio.ui.home.HomeFragment
import br.com.estrategiaaudio.ui.main.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, PresentersModule::class))
interface ApplicationComponent {
    fun inject(act: BaseActivity)
    fun inject(act: MainActivity)
    fun inject(frag: HomeFragment)
    fun inject(frag: AlbumDetailFragment)
    fun inject(frag: SeeAllAlbumsFragment)
}