package br.com.estrategiaaudio.di.application

import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import br.com.estrategiaaudio.application.MainApplication
import br.com.estrategiaaudio.network.RestClient
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class ApplicationModule(val mainApp: MainApplication) {

    @Singleton
    @Provides
    fun getApplication(): MainApplication{
        return mainApp
    }

    @Singleton
    @Provides
    fun getSharedPreferences(app: MainApplication): SharedPreferences{
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Singleton
    @Provides
    fun getRetrofit(): Retrofit{
        return RestClient.build()
    }


}
