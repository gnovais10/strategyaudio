package br.com.estrategiaaudio.di.presenters

import br.com.estrategiaaudio.ui.album.AlbumDetailPresenter
import br.com.estrategiaaudio.ui.album.SeeAllAlbumsFragment
import br.com.estrategiaaudio.ui.album.SeeAllAlbumsPresenter
import dagger.Module
import dagger.Provides
import br.com.estrategiaaudio.ui.home.HomePresenter
import br.com.estrategiaaudio.ui.main.MainActivityPresenter

@Module
class PresentersModule {

    @Provides
    fun getHomePresenter(): HomePresenter {
        return HomePresenter()
    }

    @Provides
    fun getMainAcitivityPresenter(): MainActivityPresenter {
        return MainActivityPresenter()
    }

    @Provides
    fun getAlbumDetailPresenter(): AlbumDetailPresenter {
        return AlbumDetailPresenter()
    }

    @Provides
    fun getAlbumPresenter(): SeeAllAlbumsPresenter {
        return SeeAllAlbumsPresenter()
    }


}