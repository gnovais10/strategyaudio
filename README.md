# Stratefy Android

Estratégia's Android app written in Kotlin

### Requirements

* [Kotlin](https://kotlinlang.org) (bundled in Android Studio)
* [Android Studio](https://developer.android.com/studio) 
* [Gradle](https://gradle.org/)

### Running

Run the project using Android Studio

